all: piratecyclingbook.pdf

piratecyclingbook.pdf: piratecyclingbook.tex *.tex *.sty
	xelatex $<
	xelatex $<	# to include generated ToC
	xdg-open $@

clean:
	rm -f piratecyclingbook.pdf *.{aux,toc,log,out}
